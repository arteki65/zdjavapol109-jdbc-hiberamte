create table if not exists MANUFACTURERS
(
    ID   varchar(255) not null primary key,
    NAME varchar(512) null
)
    comment 'manufacturers of cars';
