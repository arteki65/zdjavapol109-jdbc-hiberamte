CREATE TABLE IF NOT EXISTS dogs(
    ID VARCHAR(255) NOT NULL PRIMARY KEY,
    `NAME` VARCHAR(255) NOT NULL,
    AGE INT NOT NULL,
    COLOUR VARCHAR(255) NOT NULL,
    MEMBER_ID varchar(255) null,
        constraint DOGS_to_members_fk
            foreign key (MEMBER_ID) references MEMBERS (ID)
);
