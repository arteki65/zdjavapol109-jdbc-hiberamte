create table if not exists CARS
(
    ID                 varchar(255) not null primary key,
    MODEL              varchar(255) not null,
    ENGINE_TYPE        varchar(255) not null,
    YEAR_OF_PRODUCTION int          not null,
    MANUFACTURER_ID    varchar(255) null,
    constraint CARS_to_manufacturers_fk
        foreign key (MANUFACTURER_ID) references MANUFACTURERS (ID)
);
