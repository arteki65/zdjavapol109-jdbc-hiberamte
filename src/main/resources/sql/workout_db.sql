CREATE TABLE IF NOT EXISTS WORKOUTS(
    id INT AUTO_INCREMENT PRIMARY KEY,
    `type` VARCHAR(20) NOT NULL,
    `distance_km` DOUBLE NOT NULL,
    `time_HH_mm_ss`  TIME NOT NULL,
    `pace_minPerKM` DOUBLE NOT NULL,
    `speed_kmPerH`DOUBLE NOT NULL,
    `date_YYYY_MM_dd` DATE NOT NULL
);
