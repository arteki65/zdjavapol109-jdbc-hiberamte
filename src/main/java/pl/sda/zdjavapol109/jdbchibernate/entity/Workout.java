package pl.sda.zdjavapol109.jdbchibernate.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
@Entity
@Table(name = "WORKOUTS")
public class Workout {

    @Id
    private int id;
    private  WorkoutType type;
    private  double distance_km;
    private  LocalTime time_HH_mm_ss;
    private   double pace_minPerKM;
    private double speed_kmPerH;

    private  LocalDate date_YYYY_MM_dd;

    public Workout() {
    }

    public Workout(WorkoutType workoutType, double distance, LocalTime time, LocalDate date) {
        this.type = workoutType;
        this.distance_km = distance;
        this.time_HH_mm_ss = time;
        this.pace_minPerKM = calculatePace(distance,time);
        this.speed_kmPerH = calculateSpeed(distance, time);
        this.date_YYYY_MM_dd = date;
    }
    public Workout(int id, WorkoutType workoutType, double distance, LocalTime time, LocalDate date) {
        this.type = workoutType;
        this.distance_km = distance;
        this.time_HH_mm_ss = time;
        this.pace_minPerKM = calculatePace(distance,time);
        this.speed_kmPerH = calculateSpeed(distance, time);
        this.date_YYYY_MM_dd = date;
    }
    public Workout copy(int id,WorkoutType workoutType, double distance, LocalTime time, LocalDate date) {
        return new Workout(
                id,
                workoutType,
                distance,
                time,
                date
        );
    }
    public Workout(ResultSet resultSet) throws SQLException {
        this.id = resultSet.getInt("ID");
        this.type = WorkoutType.valueOf(resultSet.getString("TYPE"));
        this.distance_km = resultSet.getDouble("DISTANCE-[km]");
        this.time_HH_mm_ss = resultSet.getTime("TIME-[HH:mm:ss]").toLocalTime();
        this.pace_minPerKM = resultSet.getDouble("PACE-[min/km]");
        this.speed_kmPerH = resultSet.getDouble("SPEED-[km/h]");
        this.date_YYYY_MM_dd = resultSet.getDate("DATE-[YYYY-MM-dd]").toLocalDate();
    }





    public double calculatePace(double distance, LocalTime time){
        double minutes = time.getHour()*60 + time.getMinute() + (double)time.getSecond()/60;
        return minutes/distance;
    }
    public double calculateSpeed (double distance, LocalTime time){
        double hours = time.getHour() + (double)time.getMinute()/60 + (double)time.getSecond()/3600;
        return distance/hours;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public WorkoutType getWorkoutType() {
        return type;
    }

    public void setType(WorkoutType type) {
        this.type = type;
    }

    public double getDistance() {
        return distance_km;
    }

    public void setDistance_km(double distance_km) {
        this.distance_km = distance_km;
    }

    public LocalTime getTime() {
        return time_HH_mm_ss;
    }

    public void setTime_HH_mm_ss(LocalTime time_HH_mm_ss) {
        this.time_HH_mm_ss = time_HH_mm_ss;
    }

    public double getPace() {
        return pace_minPerKM;
    }

    public void setPace_minPerKM(double pace_minPerKM) {
        this.pace_minPerKM = pace_minPerKM;
    }

    public double getSpeed() {
        return speed_kmPerH;
    }

    public void setSpeed_kmPerH(double speed_kmPerH) {
        this.speed_kmPerH = speed_kmPerH;
    }

    public LocalDate getDate() {
        return date_YYYY_MM_dd;
    }

    public void setDate_YYYY_MM_dd(LocalDate date_YYYY_MM_dd) {
        this.date_YYYY_MM_dd = date_YYYY_MM_dd;
    }

    @Override
    public String toString() {
        return "Workout{" +
                "id=" + id +
                ", type=" + type +
                ", distance_km=" + distance_km +
                ", time_HH_mm_ss=" + time_HH_mm_ss +
                ", pace_minPerKM=" + pace_minPerKM +
                ", speed_kmPerH=" + speed_kmPerH +
                ", date_YYYY_MM_dd=" + date_YYYY_MM_dd +
                '}';
    }
}
