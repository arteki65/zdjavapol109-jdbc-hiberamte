package pl.sda.zdjavapol109.jdbchibernate.entity;

public enum DietType {
    OMNIVORE,  CARNIVORE, HERBIVORE
}
