package pl.sda.zdjavapol109.jdbchibernate.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "MEMBERS")
public class Member {

    @Id
    private String id;

    private String name;

    private String email;

    private String birthDate;

    @OneToMany(mappedBy = "member")
    private List<Dog> dogs;

    public Member(){
        //required by hibernate
    }

    public Member(String id, String name, String email, String birthDate) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.birthDate = birthDate;
    }
    public Member(String name, String email, String birthDate) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.email = email;
        this.birthDate = birthDate;
    }

    public Member(ResultSet resultSet) throws SQLException {
        this.id = resultSet.getString("ID");
        this.name = resultSet.getString("NAME");
        this.email = resultSet.getString("EMAIL");
        this.birthDate = resultSet.getString("BIRTHDATE");
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public Member copy(String name, String email, String birthDate) {
        return new Member(
                this.id,
                name,
                email,
                birthDate
        );
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public List<Dog> getDogs() {
        return dogs;
    }

    public void setDogs(List<Dog> dogs) {
        this.dogs = dogs;
    }

    @Override
    public String toString() {
        return "Member{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", dogs=" + dogs +
                '}';
    }
}

