package pl.sda.zdjavapol109.jdbchibernate.entity;

import jakarta.persistence.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

@Entity
@Table(name = "ANIMALS")
public class Animal {

    @Id
    @Column(name = "ID")
    private String animalId;

    @Column(name = "NAME")
    private String animalName;

    private String type;

    private DietType diet;

    private String colour;

    public Animal(String animalName, String type, DietType diet, String colour) {
        this.animalId = UUID.randomUUID().toString();
        this.animalName = animalName;
        this.type = type;
        this.diet = diet;
        this.colour = colour;
    }

    private Animal(String animalId, String animalName, String type, DietType diet, String colour) {
        this.animalId = animalId;
        this.animalName = animalName;
        this.type = type;
        this.diet = diet;
        this.colour = colour;
    }

    public Animal() {
        // required by hibernate
    }

    public Animal copy(String name, String type, DietType diet, String colour) {
        return new Animal(this.animalId, name, type, diet, colour);
    }

    public Animal(ResultSet resultSet) throws SQLException {
        this.animalId = resultSet.getString("ID");
        this.animalName = resultSet.getString("NAME");
        this.type = resultSet.getString("TYPE");
        this.diet = DietType.valueOf(resultSet.getString("DIET"));
        this.colour = resultSet.getString("COLOUR");
    }

    public String getAnimalId() {
        return animalId;
    }

    public String getAnimalName() {
        return animalName;
    }

    public String getType() {
        return type;
    }

    public DietType getDiet() {
        return diet;
    }

    public String getColour() {
        return colour;
    }

    public void setAnimalId(String id) {
        this.animalId = id;
    }

    public void setAnimalName(String animalName) {
        this.animalName = animalName;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDiet(DietType diet) {
        this.diet = diet;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    @Override
    public String toString() {
        return "\nAnimal{" + "id='" + animalId + '\'' + ", name='" + animalName + '\'' + ", type='" + type + '\'' + ", diet=" + diet + ", colour='" + colour + '\'' + "}";
    }
}
