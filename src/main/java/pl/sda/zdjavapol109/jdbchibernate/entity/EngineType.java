package pl.sda.zdjavapol109.jdbchibernate.entity;

public enum EngineType {
    GASOLINE, DIESEL, HYBRID, ELECTRIC
}
