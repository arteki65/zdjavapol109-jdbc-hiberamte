package pl.sda.zdjavapol109.jdbchibernate.entity;

import jakarta.persistence.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

@Entity
@Table (name = "dogs")
public class Dog {
    @Id
    private String ID;

    @ManyToOne
    @JoinColumn(name = "member_id")
    private Member member;
    private String NAME;
    private int AGE;
    private String COLOUR;

    public Dog() {
    }

    public Dog(String NAME, int AGE, String COLOUR) {
        this.ID = UUID.randomUUID().toString();
        this.NAME = NAME;
        this.AGE = AGE;
        this.COLOUR = COLOUR;
    }

    public Dog(String ID, String NAME, int AGE, String COLOUR) {
        this.ID = ID;
        this.NAME = NAME;
        this.AGE = AGE;
        this.COLOUR = COLOUR;
    }

    public Dog copy(String name, int age, String colour) {
        return new Dog(this.ID, name, age, colour);
    }

    public Dog(ResultSet resultSet) throws SQLException {
        this.ID = resultSet.getString("ID");
        this.NAME = resultSet.getString("NAME");
        this.AGE = resultSet.getInt("AGE");
        this.COLOUR = resultSet.getString("COLOUR");
    }

    public String getID() {
        return ID;
    }

    public String getNAME() {
        return NAME;
    }

    public int getAGE() {
        return AGE;
    }

    public String getCOLOUR() {
        return COLOUR;
    }


    public void setID(String ID) {
        this.ID = ID;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public void setAGE(int AGE) {
        this.AGE = AGE;
    }

    public void setCOLOUR(String COLOUR) {
        this.COLOUR = COLOUR;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "ID='" + ID + '\'' +
                ", member=" + member.getName() +
                ", NAME='" + NAME + '\'' +
                ", AGE=" + AGE +
                ", COLOUR='" + COLOUR + '\'' +
                '}';
    }
}
