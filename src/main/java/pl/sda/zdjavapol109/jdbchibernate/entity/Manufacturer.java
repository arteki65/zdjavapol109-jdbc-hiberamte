package pl.sda.zdjavapol109.jdbchibernate.entity;

import jakarta.persistence.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "MANUFACTURERS")
public class Manufacturer {

    @Id
    private String id;

    private String name;

    @OneToMany(mappedBy = "manufacturer")
    private List<Car> cars;

    public Manufacturer() {
        // required by hibernate
    }

    private Manufacturer(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Manufacturer(String name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
    }

    public Manufacturer(ResultSet rs) throws SQLException {
        this(rs.getString("MANUFACTURER_ID"), rs.getString("NAME"));
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Manufacturer{" + "id='" + id + '\'' + ", name='" + name + '\'' + ", cars=" + cars + '}';
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }
}
