package pl.sda.zdjavapol109.jdbchibernate.entity;

import jakarta.persistence.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

@Entity
@Table(name = "CARS")
public class Car {

    @Id
    private String id;

    @ManyToOne
    @JoinColumn(name = "manufacturer_id")
    private Manufacturer manufacturer;

    private String model;

    @Column(name = "ENGINE_TYPE")
    @Enumerated(EnumType.STRING)
    private EngineType engineType;

    @Column(name = "YEAR_OF_PRODUCTION")
    private int yearOfProduction;

    public Car() {
        // required by hibernate
    }

    public Car(Manufacturer manufacturer, String model, EngineType engineType, int yearOfProduction) {
        this.id = UUID.randomUUID().toString();
        this.manufacturer = manufacturer;
        this.model = model;
        this.engineType = engineType;
        this.yearOfProduction = yearOfProduction;
    }

    private Car(String id, Manufacturer manufacturer, String model, EngineType engineType, int yearOfProduction) {
        this.id = id;
        this.manufacturer = manufacturer;
        this.model = model;
        this.engineType = engineType;
        this.yearOfProduction = yearOfProduction;
    }

    public Car copy(Manufacturer manufacturer, String model, EngineType engineType, int yearOfProduction) {
        return new Car(
                this.id,
                manufacturer,
                model,
                engineType,
                yearOfProduction
        );
    }

    public Car(ResultSet resultSet, Manufacturer manufacturer) throws SQLException {
        this.id = resultSet.getString("ID");
        this.manufacturer = manufacturer;
        this.model = resultSet.getString("MODEL");
        this.engineType = EngineType.valueOf(resultSet.getString("ENGINE_TYPE"));
        this.yearOfProduction = resultSet.getInt("YEAR_OF_PRODUCTION");
    }

    public String getId() {
        return id;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public EngineType getEngineType() {
        return engineType;
    }

    public int getYearOfProduction() {
        return yearOfProduction;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setEngineType(EngineType engineType) {
        this.engineType = engineType;
    }

    public void setYearOfProduction(int yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }

    @Override
    public String toString() {
        return "Car{" + "id='" + id + '\'' + ", manufacturer='" + manufacturer.getName() + '\'' + ", model='" + model + '\''
                + ", engineType=" + engineType + ", yearOfProduction=" + yearOfProduction + '}';
    }
}
