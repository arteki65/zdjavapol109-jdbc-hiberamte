package pl.sda.zdjavapol109.jdbchibernate.entity;

public enum WorkoutType {

    WALKING("W"),
    RUNNING("R"),
    CYCLING("C"),
    SWIMMING("S");

    final String abbrv;
    WorkoutType(String abbrv) {
        this.abbrv = abbrv;
    }

    public String getAbbrv() {
        return abbrv;
    }

    public  static WorkoutType findByAbbrv(String abbrv) {
        for (WorkoutType w : WorkoutType.values()) {
            if (w.getAbbrv().equals(abbrv)) {
                return w;
            }
        }
        return null;
    }

}
