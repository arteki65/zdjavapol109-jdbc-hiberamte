package pl.sda.zdjavapol109.jdbchibernate.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

@Entity
@Table(name = "APARTMENTS")
public class Apartment {

    @Id
    private String id;

    private String location;

    private int metric_area;

    private int floor;

    private int yearOfConstruction;

    public Apartment() {
        // required by hibernate
    }

    public Apartment(String location, int metric_area, int floor, int yearOfConstruction) {
        this.id = UUID.randomUUID().toString();
        this.location = location;
        this.metric_area = metric_area;
        this.floor = floor;
        this.yearOfConstruction = yearOfConstruction;
    }

        private Apartment(String id, String location, int metric_area, int floor, int yearOfConstruction) {
        this.id = id;
        this.location = location;
        this.metric_area = metric_area;
        this.floor = floor;
        this.yearOfConstruction = yearOfConstruction;
    }

    public Apartment copy(String location, int metric_area, int floor, int yearOfConstruction) {
        return new Apartment(
                this.id,
                location,
                metric_area,
                floor,
                yearOfConstruction
        );
    }

//    public Apartment(ResultSet rs) throws SQLException {
//        this(rs.getString("APARTMENT_ID"), rs.getString("NAME"));
//    }

    public Apartment(ResultSet resultSet) throws SQLException {
        this.id = resultSet.getString("ID");
        this.location = resultSet.getString("LOCATION");
        this.metric_area = resultSet.getInt("METRIC_AREA");
        this.floor = resultSet.getInt("FLOOR");
        this.yearOfConstruction = resultSet.getInt("YEAR_OF_CONSTRUCTION");
    }

    public String getId() {
        return id;
    }

    public String getLocation() {
        return location;
    }

    public int getMetric_area() {
        return metric_area;
    }

    public int getFloor() {
        return floor;
    }

    public int getYearOfConstruction() {
        return yearOfConstruction;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setMetric_area(int metric_area) {
        this.metric_area = metric_area;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public void setYearOfConstruction(int yearOfConstruction) {
        this.yearOfConstruction = yearOfConstruction;
    }

    @Override
    public String toString() {
        return "Apartment{" +
                "id='" + id + '\'' +
                ", location='" + location + '\'' +
                ", metric_area=" + metric_area +
                ", floor=" + floor +
                ", yearOfConstruction=" + yearOfConstruction +
                '}';
    }
}

