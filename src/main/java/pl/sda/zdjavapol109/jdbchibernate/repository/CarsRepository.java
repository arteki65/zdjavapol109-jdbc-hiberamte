package pl.sda.zdjavapol109.jdbchibernate.repository;

import pl.sda.zdjavapol109.jdbchibernate.entity.Car;
import pl.sda.zdjavapol109.jdbchibernate.entity.Manufacturer;
import pl.sda.zdjavapol109.jdbchibernate.logger.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CarsRepository implements Repository<Car, String> {

    private final Connection connection;

    private final Logger logger;

    public CarsRepository(Connection connection, Logger logger) {
        this.connection = connection;
        this.logger = logger;
    }

    // Create
    @Override
    public void create(Car car) throws SQLException {
        PreparedStatement selectManufacturerPS = connection.prepareStatement(
                "SELECT * FROM MANUFACTURERS WHERE NAME=?");
        selectManufacturerPS.setString(1, car.getManufacturer().getName());
        ResultSet manufacturerRS = selectManufacturerPS.executeQuery();
        boolean manufacturerExists = manufacturerRS.next();

        if (!manufacturerExists) {
            PreparedStatement insertManufacturerPS = connection.prepareStatement(
                    "INSERT INTO MANUFACTURERS(ID, NAME) VALUES(?, ?)");
            insertManufacturerPS.setString(1, car.getManufacturer().getId());
            insertManufacturerPS.setString(2, car.getManufacturer().getName());

            insertManufacturerPS.execute();

            PreparedStatement insertCarPS = connection.prepareStatement(
                    "INSERT INTO CARS(ID, MANUFACTURER_ID, MODEL, ENGINE_TYPE, YEAR_OF_PRODUCTION)"
                            + " VALUES(?, ?, ?, ?, ?)");
            insertCarPS.setString(1, car.getId());
            insertCarPS.setString(2, car.getManufacturer().getId());
            insertCarPS.setString(3, car.getModel());
            insertCarPS.setString(4, car.getEngineType().toString());
            insertCarPS.setInt(5, car.getYearOfProduction());

            logger.log("Going to execute SQL:\n" + insertCarPS);
            insertCarPS.execute();
            logger.log("SQL executed successfully!");
            return;
        }

        // manufacturer already exists
        PreparedStatement insertCarPS = connection.prepareStatement(
                "INSERT INTO CARS(ID, MANUFACTURER_ID, MODEL, ENGINE_TYPE, YEAR_OF_PRODUCTION)"
                        + " VALUES(?, ?, ?, ?, ?)");
        insertCarPS.setString(1, car.getId());
        insertCarPS.setString(2, manufacturerRS.getString("ID"));
        insertCarPS.setString(3, car.getModel());
        insertCarPS.setString(4, car.getEngineType().toString());
        insertCarPS.setInt(5, car.getYearOfProduction());

        logger.log("Going to execute SQL:\n" + insertCarPS);
        insertCarPS.execute();
        logger.log("SQL executed successfully!");
    }

    public List<Car> findAll() throws SQLException {
        return findAll(100);
    }

    // Read
    @Override
    public List<Car> findAll(int limit) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(
                "SELECT * FROM CARS LEFT JOIN MANUFACTURERS M on M.ID = CARS.MANUFACTURER_ID LIMIT ?");
        ps.setInt(1, limit);
        ResultSet resultSet = ps.executeQuery();

        final List<Car> listOfCars = new ArrayList<>();
        while (resultSet.next()) {
            listOfCars.add(new Car(resultSet, new Manufacturer(resultSet)));
        }
        return listOfCars;
    }

    @Override
    public Car findById(String id) throws SQLException {
        /*PreparedStatement ps = connection.prepareStatement("SELECT * FROM CARS WHERE ID=?");
        ps.setString(1, id);

        logger.log("Going to execute SQL:\n" + ps);
        ResultSet resultSet = ps.executeQuery();
        logger.log("SQL executed successfully!");

        resultSet.next();
        return new Car(resultSet);*/
        throw new UnsupportedOperationException();
    }

    public List<Car> findByManufacturer(String manufacturer) throws SQLException {
        /*PreparedStatement ps = connection.prepareStatement("SELECT * FROM CARS WHERE MANUFACTURER=?");
        ps.setString(1, manufacturer);

        logger.log("Going to execute SQL:\n" + ps);
        ResultSet resultSet = ps.executeQuery();
        logger.log("SQL executed successfully!");

        final List<Car> listOfCars = new ArrayList<>();
        while (resultSet.next()) {
            listOfCars.add(new Car(resultSet));
        }
        return listOfCars;*/
        throw new UnsupportedOperationException();
    }

    public List<Car> findWhereYearOfProductionIsAfter(int yearOfProduction) throws SQLException {
        /*String sql = String.format("SELECT * FROM CARS WHERE YEAR_OF_PRODUCTION>'%d'", yearOfProduction);

        System.out.println("Going to execute SQL:\n" + sql);
        ResultSet resultSet = connection.createStatement().executeQuery(sql);
        System.out.println("SQL executed successfully!");

        final List<Car> listOfCars = new ArrayList<>();
        while (resultSet.next()) {
            listOfCars.add(new Car(resultSet));
        }
        return listOfCars;*/
        throw new UnsupportedOperationException();
    }

    // Update
    @Override
    public void update(Car car) throws SQLException {
        String sql = String.format(
                "UPDATE CARS SET YEAR_OF_PRODUCTION='%d', MODEL='%s', ENGINE_TYPE='%s', MANUFACTURER='%s' WHERE ID='%s'",
                car.getYearOfProduction(), car.getModel(), car.getEngineType(), car.getManufacturer(), car.getId());

        logger.log("Going to execute SQL:\n" + sql);
        final int updatedRows = connection.createStatement().executeUpdate(sql);
        logger.log("Updated " + updatedRows + " cars!");
    }

    // Delete
    @Override
    public void delete(Car car) throws SQLException {
        deleteById(car.getId());
    }

    @Override
    public void deleteById(String id) throws SQLException {
        String sql = String.format("DELETE FROM CARS WHERE ID='%s'", id);

        logger.log("Going to execute SQL:\n" + sql);
        final int deletedRows = connection.createStatement().executeUpdate(sql);
        logger.log("Deleted " + deletedRows + " cars!");
    }
}
