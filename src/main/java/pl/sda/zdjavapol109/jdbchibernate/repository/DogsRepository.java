package pl.sda.zdjavapol109.jdbchibernate.repository;
import pl.sda.zdjavapol109.jdbchibernate.entity.Dog;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DogsRepository implements Repository<Dog, String> {

    private final Connection connection;

    public DogsRepository(Connection connection) {
        this.connection = connection;
    }


    @Override
    public void create(Dog dog) throws SQLException {
        String insertSql = "INSERT INTO dogs(ID, `NAME`, AGE, COLOUR) VALUES('%s', '%s', %d, '%s')";
        String sql = String.format(insertSql, dog.getID(), dog.getNAME(), dog.getAGE(), dog.getCOLOUR());
        System.out.println("Going to execute SQL:\n" + sql);
        connection.createStatement().execute(sql);
        System.out.println("SQL executed successfully!");
    }


    public List<Dog> findAll() throws SQLException {
        return findAll(100);
    }


    @Override
    public List<Dog> findAll(int limit) throws SQLException {
        String sql = "SELECT * FROM dogs LIMIT " + limit;
        System.out.println("Going to execute SQL:\n" + sql);
        ResultSet resultSet = connection.createStatement().executeQuery(sql);
        System.out.println("SQL executed successfully!");
        final List<Dog> listOfDogs = new ArrayList<>();
        while (resultSet.next()) {
            listOfDogs.add(new Dog(resultSet));
        }
        return listOfDogs;
    }


    @Override
    public Dog findById(String id) throws SQLException {
        String sql = String.format("SELECT * FROM dogs WHERE ID = '%s'", id);
        System.out.println("Going to execute SQL:\n" + sql);
        ResultSet resultSet = connection.createStatement().executeQuery(sql);
        System.out.println("SQL executed successfully!");

        resultSet.next();
        return new Dog(resultSet);
    }


    @Override
    public void update(Dog dog) throws SQLException {
        String stringSql = "UPDATE dogs SET NAME = '%s', AGE = '%d', COLOUR = '%s' WHERE ID = '%s'";
        String sql = String.format(stringSql, dog.getNAME(), dog.getAGE(), dog.getCOLOUR(), dog.getID());
        System.out.println("Going to execute SQL:\n" + sql);
        final int updatedRows = connection.createStatement().executeUpdate(sql);
        if (updatedRows == 1) {
            System.out.println("Updated " + updatedRows + " dog!");
        } else {
            System.out.println("Updated " + updatedRows + " dogs!");
        }
    }

    @Override
    public void delete(Dog dog) throws SQLException {
        deleteById(dog.getID());
    }

    @Override
    public void deleteById(String id) throws SQLException {
        String sql = String.format("DELETE FROM dogs WHERE ID = '%s'", id);
        System.out.println("Going to execute SQL:\n" + sql);
        final int deletedRows = connection.createStatement().executeUpdate(sql);
        System.out.println("Deleted " + deletedRows + " dogs!");
    }
}

