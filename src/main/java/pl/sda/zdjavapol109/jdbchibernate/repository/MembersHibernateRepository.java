package pl.sda.zdjavapol109.jdbchibernate.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.TypedQuery;
import pl.sda.zdjavapol109.jdbchibernate.entity.Member;

import java.util.List;
import java.util.Optional;

public class MembersHibernateRepository implements Repository<Member, String> {

    private final EntityManager em;

    public MembersHibernateRepository(EntityManager em) {
        this.em = em;
    }

    @Override
    public void create(Member member) {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        em.persist(member);
        transaction.commit();
    }

    @Override
    public List<Member> findAll(int limit) {
        TypedQuery<Member> query = em.createQuery("from Member", Member.class);

        return query.setMaxResults(limit).getResultList();
    }

    @Override
    public Member findById(String id) {return em.find(Member.class, id); }

    @Override
    public void update(Member member) {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        em.merge(member);

        transaction.commit();
    }

    @Override
    public void delete(Member member) {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        em.remove(member);

        transaction.commit();
    }

    @Override
    public void deleteById(String id) {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        Optional<Member> member = Optional.ofNullable(em.find(Member.class, id));
        member.ifPresent(em::remove);

        transaction.commit();
    }
}
