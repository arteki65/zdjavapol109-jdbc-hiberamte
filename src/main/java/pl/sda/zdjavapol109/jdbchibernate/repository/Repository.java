package pl.sda.zdjavapol109.jdbchibernate.repository;

import java.sql.SQLException;
import java.util.List;

public interface Repository<T, IDType> {

    void create(T object) throws SQLException;

    List<T> findAll(int limit) throws SQLException;

    T findById(IDType id) throws SQLException;

    void update(T object) throws SQLException;

    void delete(T object) throws SQLException;

    void deleteById(IDType id) throws SQLException;
}
