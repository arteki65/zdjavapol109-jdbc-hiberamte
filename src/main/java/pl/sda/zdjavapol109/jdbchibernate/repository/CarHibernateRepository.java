package pl.sda.zdjavapol109.jdbchibernate.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.persistence.Tuple;
import jakarta.persistence.TypedQuery;
import pl.sda.zdjavapol109.jdbchibernate.entity.Car;
import pl.sda.zdjavapol109.jdbchibernate.entity.EngineType;

import java.sql.SQLException;
import java.util.List;

public class CarHibernateRepository implements Repository<Car, String> {

    private final EntityManager em;

    public CarHibernateRepository(EntityManager em) {
        this.em = em;
    }

    @Override
    public void create(Car object) throws SQLException {

    }

    public List<Object[]> findCarsManufacturerNameAndModelOnly(int limit) {
        TypedQuery<Object[]> query = em.createQuery("select manufacturer.name, model from Car", Object[].class);
        return query.setMaxResults(limit).getResultList();
    }

    public List<Car> findCarsByModel(String model) {
        TypedQuery<Car> query = em.createQuery("from Car where model = :model", Car.class);
        query.setParameter("model", model);
        return query.getResultList();
    }

    public List<Car> findCarsByManufacturer(String manufacturer) {
        TypedQuery<Car> query = em.createQuery("from Car where manufacturer.name = :manufacturer", Car.class);
        query.setParameter("manufacturer", manufacturer);
        return query.getResultList();
    }

    public List<Car> findAllOrderByYearOfProduction(int limit, boolean asc) {
        TypedQuery<Car> query = em.createQuery("from Car order by yearOfProduction " + (asc ? "asc" : "desc"),
                Car.class);
        return query.setMaxResults(limit).getResultList();
    }

    public List<Object[]> findAvgYearOfProductionByManufacturers() {
        TypedQuery<Object[]> query = em.createQuery(
                "select manufacturer.name, avg(yearOfProduction) from Car group by manufacturer", Object[].class);
        return query.getResultList();
    }

    @Override
    public List<Car> findAll(int limit) throws SQLException {
        TypedQuery<Car> query = em.createQuery("from Car", Car.class);
        return query.setMaxResults(limit).getResultList();
    }

    @Override
    public Car findById(String id) throws SQLException {
        return null;
    }

    @Override
    public void update(Car object) throws SQLException {

    }

    @Override
    public void delete(Car object) throws SQLException {

    }

    @Override
    public void deleteById(String id) throws SQLException {

    }
}
