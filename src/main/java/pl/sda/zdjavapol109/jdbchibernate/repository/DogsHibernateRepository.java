package pl.sda.zdjavapol109.jdbchibernate.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.TypedQuery;
import pl.sda.zdjavapol109.jdbchibernate.entity.Dog;
import pl.sda.zdjavapol109.jdbchibernate.entity.Manufacturer;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class DogsHibernateRepository implements Repository<Dog, String> {

    private final EntityManager em;

    public DogsHibernateRepository(EntityManager em) {
        this.em = em;
    }


    @Override
    public void create(Dog dog) throws SQLException {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        em.persist(dog);

        transaction.commit();
    }

    @Override
    public List<Dog> findAll(int limit) {
        TypedQuery<Dog> query = em.createQuery("from Dog", Dog.class);

        return query.setMaxResults(limit).getResultList();
    }

    @Override
    public Dog findById(String id) {
        return em.find(Dog.class, id);
    }

    @Override
    public void update(Dog dog) {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        em.merge(dog);

        transaction.commit();
    }

    @Override
    public void delete(Dog dog) {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        em.remove(dog);

        transaction.commit();
    }

    @Override
    public void deleteById(String id) {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        Optional<Dog> dog = Optional.ofNullable(em.find(Dog.class, id));
        dog.ifPresent(em::remove);

        transaction.commit();
    }
}
