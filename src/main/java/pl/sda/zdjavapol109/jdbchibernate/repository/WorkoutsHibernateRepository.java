package pl.sda.zdjavapol109.jdbchibernate.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.TypedQuery;
import pl.sda.zdjavapol109.jdbchibernate.entity.Manufacturer;
import pl.sda.zdjavapol109.jdbchibernate.entity.Workout;
import pl.sda.zdjavapol109.jdbchibernate.entity.WorkoutType;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class WorkoutsHibernateRepository implements Repository<Workout, Integer>, AnotherRepository<Workout,Integer> {


    private final EntityManager em;
    private static final Scanner SCANNER = new Scanner(System.in);

    public WorkoutsHibernateRepository(EntityManager em) {

        this.em = em;
    }



    @Override
    public void create(Workout workout) throws SQLException {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        em.persist(workout);

        transaction.commit();
    }

    @Override
    public List<Workout> findAll(int limit) throws SQLException {
        TypedQuery<Workout> query = em.createQuery("from Workout", Workout.class);

        return query.setMaxResults(limit).getResultList();
    }

    @Override
    public Workout findById(Integer id) throws SQLException {
        return em.find(Workout.class, id);
    }

    @Override
    public void update(Workout workout) throws SQLException {

        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        em.merge(workout);

        transaction.commit();

    }

    @Override
    public void delete(Workout workout) throws SQLException {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        em.remove(workout);

        transaction.commit();
    }

    @Override
    public void deleteById(Integer id) throws SQLException {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        Optional<Workout> workout = Optional.ofNullable(em.find(Workout.class, id));
        workout.ifPresent(em::remove);

        transaction.commit();
    }


    public WorkoutType getWorkoutTypeFromUserInput() {
        WorkoutType workoutType;
        System.out.println("Choose from available workout types (abbreviations allowed):");
        for (WorkoutType s: WorkoutType.values()){
            System.out.print(s + "-> ["+s.getAbbrv()+"],  " );
        }
        String tmpWorkoutType = SCANNER.nextLine();
        if (tmpWorkoutType.length() == 1) {
            workoutType = WorkoutType.findByAbbrv(tmpWorkoutType.toUpperCase());
        } else {
            workoutType = WorkoutType.valueOf(tmpWorkoutType.toUpperCase());
        }
        return workoutType;
    }

    public double getDistanceFromUserInput() {
        System.out.println("Insert workout distance [km] ");
        double distance = SCANNER.nextDouble();
        SCANNER.nextLine();
        return distance;
    }

    public LocalTime getTimeFromUserInput() {
        System.out.println("Insert workout duration time [HH:mm:ss] ");
        LocalTime time = LocalTime.parse(SCANNER.next());
        return time;
    }

    public LocalDate getDateFromUserInput() {
        System.out.println("Insert workout date [YYYY-MM-DD], or leave this field empty to insert current date: ");
        String tmp = SCANNER.nextLine();
        LocalDate date;
        if (!tmp.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}")) {
            date = LocalDate.now();
        } else {
            date = LocalDate.parse(tmp);
        }
        return date;
    }

    @Override
    public void update(Workout workout, int id) throws SQLException {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();
        workout.setId(id);
        em.merge(workout);

        transaction.commit();
    }
}
