package pl.sda.zdjavapol109.jdbchibernate.repository;

import pl.sda.zdjavapol109.jdbchibernate.entity.Animal;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AnimalsRepository implements Repository<Animal, String> {
    private final Connection connection;

    public AnimalsRepository(Connection connection) {
        this.connection = connection;
    }

    //Create
    @Override
    public void create(Animal animal) throws SQLException {
        String insertSql = "INSERT INTO ANIMALS(ID, NAME, TYPE, DIET, COLOUR)" + "VALUES('%s', '%s', '%s', '%s', '%s')";
        String sql = String.format(insertSql, animal.getAnimalId(), animal.getAnimalName(), animal.getType(), animal.getDiet(), animal.getColour());

        System.out.println("Going to execute SQL:\n" + sql);
        connection.createStatement().execute(sql);
        System.out.println("SQL executed successfully!");
    }

    //Read
    public List<Animal> findAll() throws SQLException {
        return findAll(100);
    }

    @Override
    public List<Animal> findAll(int limit) throws SQLException {
        String sql = "SELECT * FROM ANIMALS LIMIT" + limit;

        System.out.println("Going to execute SQL:\n" + sql);
        ResultSet resultSet = connection.createStatement().executeQuery(sql);
        System.out.println("SQL executed successfully!");

        final List<Animal> listOfAnimals = new ArrayList<>();
        while (resultSet.next()) {
            listOfAnimals.add(new Animal(resultSet));
        }
        return listOfAnimals;
    }

    @Override
    public Animal findById(String id) throws SQLException {
        String sql = String.format("SELECT * FROM ANIMALS WHERE ID='%s'", id);
        System.out.println("Going to execute SQL:\n" + sql);
        ResultSet resultSet = connection.createStatement().executeQuery(sql);
        System.out.println("SQL executed successfully!");

        resultSet.next();
        return new Animal(resultSet);
    }

    //Update
    @Override
    public void update(Animal animal) throws SQLException {
        String sql = String.format(
                "UPDATE ANIMALS SET NAME='%S', TYPE='%S', DIET='%S', COLOUR='%S' WHERE ID='%s'",
                animal.getAnimalName(), animal.getType(), animal.getDiet(), animal.getColour(), animal.getAnimalId());

        System.out.println("Going to execute SQL:\n" + sql);
        final int updatedRows = connection.createStatement().executeUpdate(sql);
        System.out.println("Updated " + updatedRows + " animals!");

    }

    //Delete
    @Override
    public void delete(Animal animal) throws SQLException {
        deleteById(animal.getAnimalId());
    }

    @Override
    public void deleteById(String id) throws SQLException {
        String sql = String.format("DELETE FROM ANIMALS WHERE ID='%s'", id);

        System.out.println("Going to execute SQL:\n" + sql);
        final int deletedRows = connection.createStatement().executeUpdate(sql);
        System.out.println("Deleted " + deletedRows + " animals!");
    }
}
