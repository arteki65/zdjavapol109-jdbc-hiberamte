/*
package pl.sda.zdjavapol109.jdbchibernate.repository;

import pl.sda.zdjavapol109.jdbchibernate.entity.Car;
import pl.sda.zdjavapol109.jdbchibernate.entity.Workout;
import pl.sda.zdjavapol109.jdbchibernate.entity.WorkoutType;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;


public class WorkoutsRepository implements Repository<Workout, Integer>, AnotherRepository<Workout, Integer> {
    private static final Scanner SCANNER = new Scanner(System.in);
    private final Connection connection;

    public WorkoutsRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void create(Workout workout) throws SQLException {
        String insertSql = "INSERT INTO WORKOUTS (TYPE, `DISTANCE-[km]`, `TIME-[HH:mm:ss]`, `PACE-[min/km]`, `SPEED-[km/h]`, `DATE-[YYYY-MM-dd]`)"
                + "VALUES ('%s', %.2f, \"%s\",%.2f, %.2f, '%s')";

        String sql = String.format(Locale.US, insertSql, workout.getWorkoutType(), workout.getDistance(), workout.getTime(), workout.getPace(), workout.getSpeed(), workout.getDate());
        System.out.println("Going to execute SQL:\n" + sql);
        connection.createStatement().execute(sql);
        System.out.println("SQL executed successfully!");
    }

    public List<Workout> findAll() throws SQLException {
        return findAll(100);
    }

    @Override
    public List<Workout> findAll(int limit) throws SQLException {
        String sql = "SELECT * FROM workouts LIMIT " + limit;

        System.out.println("Going to execute SQL:\n" + sql);
        ResultSet resultSet = connection.createStatement().executeQuery(sql);
        System.out.println("SQL executed successfully!");

        final List<Workout> listOfWorkouts = new ArrayList<>();
        while (resultSet.next()) {
            listOfWorkouts.add(new Workout(resultSet));
        }
        return listOfWorkouts;
    }

    @Override
    public Workout findById(Integer id) throws SQLException {

        String sql = String.format("SELECT * FROM workouts WHERE ID='%s'", id);

        System.out.println("Going to execute SQL:\n" + sql);
        ResultSet resultSet = connection.createStatement().executeQuery(sql);
        System.out.println("SQL executed successfully!");

        resultSet.next();
        return new Workout(resultSet);

    }

    @Override
    public void update(Workout object) throws SQLException {

    }



    @Override
    public void update(Workout workout, int id) throws SQLException {
        String sql = String.format(Locale.US,
                "UPDATE workouts SET `TYPE`='%s', `DISTANCE-[km]`=%.2f, `TIME-[HH:mm:ss]`=\"%s\", `DATE-[YYYY-MM-dd]`='%s' WHERE ID='%d'",
                workout.getWorkoutType(), workout.getDistance(), workout.getTime(), workout.getDate(), id);

        System.out.println("Going to execute SQL:\n" + sql);
        final int updatedRows = connection.createStatement().executeUpdate(sql);
        System.out.println("Updated " + updatedRows + " workouts!");
    }


    public WorkoutType getWorkoutTypeFromUserInput() {
        WorkoutType workoutType;
        System.out.println("Choose from available workout types (abbreviations allowed):");
        for (WorkoutType s: WorkoutType.values()){
            System.out.print(s + "-> ["+s.getAbbrv()+"],  " );
        }
        String tmpWorkoutType = SCANNER.nextLine();
        if (tmpWorkoutType.length() == 1) {
            workoutType = WorkoutType.findByAbbrv(tmpWorkoutType.toUpperCase());
        } else {
            workoutType = WorkoutType.valueOf(tmpWorkoutType.toUpperCase());
        }
        return workoutType;
    }

    public double getDistanceFromUserInput() {
        System.out.println("Insert workout distance [km] ");
        double distance = SCANNER.nextDouble();
        return distance;
    }

    public LocalTime getTimeFromUserInput() {
        System.out.println("Insert workout duration time [HH:mm:ss] ");
        LocalTime time = LocalTime.parse(SCANNER.next());
        return time;
    }

    public LocalDate getDateFromUserInput() {
        System.out.println("Insert workout date [YYYY-MM-DD], or leave this field empty to insert current date: ");
        SCANNER.nextLine();
        String tmp = SCANNER.nextLine();
        LocalDate date;
        if (!tmp.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}")) {
            date = LocalDate.now();
        } else {
            date = LocalDate.parse(tmp);
        }
        return date;
    }

    private static Workout createWorkoutFromUserInput(WorkoutsRepository workoutsRepository) throws SQLException {

        WorkoutType workoutType = workoutsRepository.getWorkoutTypeFromUserInput();
        double distance = workoutsRepository.getDistanceFromUserInput();
        LocalTime time = workoutsRepository.getTimeFromUserInput();
        LocalDate date = workoutsRepository.getDateFromUserInput();
        return new Workout(workoutType, distance, time, date);
    }


    @Override
    public void delete(Workout workout) throws SQLException {
        deleteById(workout.getId());
    }

    @Override
    public void deleteById(Integer id) throws SQLException {
        String sql = String.format("DELETE FROM workouts WHERE ID=%d", id);

        System.out.println("Going to execute SQL:\n" + sql);
        final int deletedRows = connection.createStatement().executeUpdate(sql);
        System.out.println("Deleted " + deletedRows + " workouts!");
    }
}
*/
