package pl.sda.zdjavapol109.jdbchibernate.repository;

import pl.sda.zdjavapol109.jdbchibernate.entity.Apartment;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ApartmentsRepository implements Repository<Apartment, String> {

    private final Connection connection;

    public ApartmentsRepository(Connection connection) {
        this.connection = connection;
    }

    // Create
    @Override
    public void create(Apartment apartment) throws SQLException {
        String insertSql = "INSERT INTO APARTMENTS(ID, LOCATION, METRIC_AREA, FLOOR, YEAR_OF_CONSTRUCTION)"
                + "VALUES('%s', '%s', '%d', '%d', '%d')";
        String sql = String.format(insertSql, apartment.getId(), apartment.getLocation(), apartment.getMetric_area(),
                apartment.getFloor(), apartment.getYearOfConstruction());

        System.out.println("Going to execute SQL:\n" + sql);
        connection.createStatement().execute(sql);
        System.out.println("SQL executed successfully!");
    }

    public List<Apartment> findAll() throws SQLException {
        return findAll(100);
    }

    // Read
    @Override
    public List<Apartment> findAll(int limit) throws SQLException {
        String sql = "SELECT * FROM APARTMENTS LIST" + limit;

        System.out.println("Going to execute SQL:\n" + sql);
        ResultSet resultSet = connection.createStatement().executeQuery(sql);
        System.out.println("SQL executed successfully!");

        final List<Apartment> listOfApartments = new ArrayList<>();
        while (resultSet.next()) {
            listOfApartments.add(new Apartment(resultSet));
        }
        return listOfApartments;
    }

    @Override
    public Apartment findById(String id) throws SQLException {
        String sql = String.format("SELECT * FROM APARTMENTS WHERE ID= '%s'", id);

        System.out.println("Going to execute SQL:\n" + sql);
        ResultSet resultSet = connection.createStatement().executeQuery(sql);
        System.out.println("SQL executed successfully!");

        resultSet.next();
        return new Apartment(resultSet);
    }

    // Update
    @Override
    public void update(Apartment apartment) throws SQLException {
        String sql = String.format(
                "UPDATE APARTMENTS SET YEAR_OF_CONSTRUCTION='%d', METRIC_AREA='%d', FLOOR='%d', LOCATION='%s' WHERE ID='%s'",
                apartment.getYearOfConstruction(), apartment.getMetric_area(), apartment.getFloor(),
                apartment.getLocation(), apartment.getId());

        System.out.println("Going to execute SQL:\n" + sql);
        final int updatedRows = connection.createStatement().executeUpdate(sql);
        System.out.println("Updated " + updatedRows + " cars!");
    }

    // Delete
    @Override
    public void delete(Apartment apartment) throws SQLException {
        deleteById(apartment.getId());
    }

    @Override
    public void deleteById(String id) throws SQLException {
        String sql = String.format("DELETE FROM APARTMENTS WHERE ID='%s'", id);

        System.out.println("Going to execute SQL:\n" + sql);
        final int updatedRows = connection.createStatement().executeUpdate(sql);
        System.out.println("Updated " + updatedRows + " cars!");
    }
}
