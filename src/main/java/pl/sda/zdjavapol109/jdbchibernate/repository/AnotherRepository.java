package pl.sda.zdjavapol109.jdbchibernate.repository;

import java.sql.SQLException;

public interface AnotherRepository<T, IDType> {
    void update(T object, int id) throws SQLException;
}
