package pl.sda.zdjavapol109.jdbchibernate.repository;

import pl.sda.zdjavapol109.jdbchibernate.entity.Member;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MembersRepository implements Repository<Member, String> {

    private final Connection connection;

    public MembersRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void create(Member member) throws SQLException {
        String insertSql = "INSERT INTO MEMBERS(ID, NAME, EMAIL, BIRTHDATE)"
                + "VALUES('%s', '%s', '%s', '%s')";
        String sql = String.format(insertSql, member.getId(), member.getName(), member.getEmail(), member.getBirthDate());

        System.out.println("Going to execute SQL:\n" + sql);
        connection.createStatement().execute(sql);
        System.out.println("SQL executed successfully!");
    }

    @Override
    public List<Member> findAll(int limit) throws SQLException {
        String sql = "SELECT * FROM MEMBERS LIMIT " + limit;

        System.out.println("Going to execute SQL:\n" + sql);
        ResultSet resultSet = connection.createStatement().executeQuery(sql);
        System.out.println("SQL executed successfully!");

        final List<Member> listOfMembers = new ArrayList<>();
        while (resultSet.next()) {
            listOfMembers.add(new Member(resultSet));
        }
        return listOfMembers;
    }

    @Override
    public Member findById(String id) throws SQLException {
        String sql = String.format("SELECT * FROM MEMBERS WHERE ID='%s'", id);

        System.out.println("Going to execute SQL:\n" + sql);
        ResultSet resultSet = connection.createStatement().executeQuery(sql);
        System.out.println("SQL executed successfully!");

        resultSet.next();
        return new Member(resultSet);
    }

    @Override
    public void update(Member member) throws SQLException {
        String sql = String.format(
                "UPDATE MEMBERS2 SET NAME='%s', EMAIL='%s', BIRTHDATE='%s' WHERE ID='%s'",
                member.getName(), member.getEmail(), member.getBirthDate(), member.getId());

        System.out.println("Going to execute SQL:\n" + sql);
        final int updatedRows = connection.createStatement().executeUpdate(sql);
        System.out.println("Updated " + updatedRows + " members!");
    }

    @Override
    public void delete(Member member) throws SQLException {
        deleteById(member.getId());
    }

    @Override
    public void deleteById(String id) throws SQLException {
        String sql = String.format("DELETE FROM MEMBERS WHERE ID='%s'", id);

        System.out.println("Going to execute SQL:\n" + sql);
        final int deletedRows = connection.createStatement().executeUpdate(sql);
        System.out.println("Deleted " + deletedRows + " members!");
    }
}
