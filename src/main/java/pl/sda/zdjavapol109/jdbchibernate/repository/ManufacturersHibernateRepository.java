package pl.sda.zdjavapol109.jdbchibernate.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.TypedQuery;
import pl.sda.zdjavapol109.jdbchibernate.entity.Manufacturer;

import java.util.List;
import java.util.Optional;

public class ManufacturersHibernateRepository implements Repository<Manufacturer, String> {

    private final EntityManager em;

    public ManufacturersHibernateRepository(EntityManager em) {
        this.em = em;
    }

    @Override
    public void create(Manufacturer manufacturer) {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        em.persist(manufacturer);

        transaction.commit();
    }

    @Override
    public List<Manufacturer> findAll(int limit) {
        TypedQuery<Manufacturer> query = em.createQuery("from Manufacturer", Manufacturer.class);

        return query.setMaxResults(limit).getResultList();
    }

    @Override
    public Manufacturer findById(String id) {
        return em.find(Manufacturer.class, id);
    }

    @Override
    public void update(Manufacturer manufacturer) {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        em.merge(manufacturer);

        transaction.commit();
    }

    @Override
    public void delete(Manufacturer manufacturer) {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        em.remove(manufacturer);

        transaction.commit();
    }

    @Override
    public void deleteById(String id) {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        Optional<Manufacturer> manufacturer = Optional.ofNullable(em.find(Manufacturer.class, id));
        manufacturer.ifPresent(em::remove);

        transaction.commit();
    }
}
