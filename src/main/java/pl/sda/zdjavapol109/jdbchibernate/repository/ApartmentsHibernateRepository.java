package pl.sda.zdjavapol109.jdbchibernate.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.TypedQuery;
import pl.sda.zdjavapol109.jdbchibernate.entity.Apartment;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class ApartmentsHibernateRepository implements Repository<Apartment, String> {

    private final EntityManager em;

    public ApartmentsHibernateRepository(EntityManager em) {
        this.em = em;
    }

    @Override
    public void create(Apartment apartment) throws SQLException {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        em.persist(apartment);

        transaction.commit();
    }

    @Override
    public List<Apartment> findAll(int limit) throws SQLException {
        TypedQuery<Apartment> query = em.createQuery("from Apartment", Apartment.class);

        return query.setMaxResults(limit).getResultList();
    }

    @Override
    public Apartment findById(String id) throws SQLException {
        return em.find(Apartment.class, id);
    }

    @Override
    public void update(Apartment apartment) throws SQLException {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        em.merge(apartment);

        transaction.commit();

    }

    @Override
    public void delete(Apartment apartment) throws SQLException {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        em.remove(apartment);

        transaction.commit();
    }

    @Override
    public void deleteById(String id) throws SQLException {
        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        Optional<Apartment> apartment = Optional.ofNullable(em.find(Apartment.class, id));
        apartment.ifPresent(em::remove);

        transaction.commit();

    }
}
