package pl.sda.zdjavapol109.jdbchibernate;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class DbInitializer {

    private final Connection connection;

    public DbInitializer(Connection connection) {
        this.connection = connection;
    }

    public void initDb() throws IOException, SQLException {
        try (InputStream carsResource = getClass().getResourceAsStream("/sql/cars_db.sql");
             InputStream apartmentsResource = getClass().getResourceAsStream("/sql/apartments_db.sql");
             InputStream workoutsResource = getClass().getResourceAsStream("/sql/workout_db.sql");
                InputStream manufacturersResource = getClass().getResourceAsStream("/sql/manufacturers_db.sql");
                InputStream membersResource = getClass().getResourceAsStream("/sql/members_db.sql")) {

            executeSqlFromResource(manufacturersResource);
            executeSqlFromResource(carsResource);
            executeSqlFromResource(apartmentsResource);
            executeSqlFromResource(workoutsResource);
            executeSqlFromResource(membersResource);
        }
    }

    public void initDbForDogs() throws IOException, SQLException {
        try (InputStream resourceAsStream = getClass().getResourceAsStream("/sql/dogs_db.sql")) {
            if (resourceAsStream == null) {
                System.err.println("Failed to open sql/dogs_db.sql");
                return;
            }
            String sql = new String(resourceAsStream.readAllBytes());

            System.out.println("Going to execute sql: \n" + sql);

            Statement statement = connection.createStatement();
            statement.execute(sql);

            System.out.println("SQL executed successfully!");
        }
    }

    private void executeSqlFromResource(InputStream resourceStream) throws IOException, SQLException {
        if (resourceStream == null) {
            System.err.println("Failed to open resource!");
            return;
        }
        String sql = new String(resourceStream.readAllBytes());

        System.out.println("Going to execute sql: \n" + sql);

        PreparedStatement ps = connection.prepareStatement(sql);
        ps.execute();

        System.out.println("SQL executed successfully!");
    }
}
