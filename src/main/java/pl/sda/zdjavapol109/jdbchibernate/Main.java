package pl.sda.zdjavapol109.jdbchibernate;

import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import pl.sda.zdjavapol109.jdbchibernate.entity.*;
import pl.sda.zdjavapol109.jdbchibernate.repository.*;
import pl.sda.zdjavapol109.jdbchibernate.logger.ConsoleLogger;
import pl.sda.zdjavapol109.jdbchibernate.repository.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {

    public static final String JDBC_URL = "jdbc:mysql://localhost:3306/";

    public static final String DB_NAME_ENV = "DB_NAME";

    public static final String DB_USER_ENV = "DB_USER";

    public static final String DB_PASSWORD_ENV = "DB_PASSWORD";

    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) throws IOException, SQLException {
        System.out.println("JDBC world! Going to connect to DB...");

        try (final Connection connection = DriverManager.getConnection(JDBC_URL + System.getenv(DB_NAME_ENV),
                System.getenv(DB_USER_ENV), System.getenv(DB_PASSWORD_ENV));
                /*SessionFactory sessionFactory = new Configuration()
                        .addAnnotatedClass(Manufacturer.class)
                        .addAnnotatedClass(Dog.class)
                        .addAnnotatedClass(Apartment.class)
                        .addAnnotatedClass(Workout.class)
                        .configure("/hibernate/hibernate.cfg.xml")
                        .buildSessionFactory()*/) {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("sda-zdjavapol109");
            System.out.println("Connected to DB!");
            System.out.println("Hibernate is running as JPA provider " + emf);

            final DbInitializer dbInitializer = new DbInitializer(connection);
            dbInitializer.initDb();
            dbInitializer.initDbForDogs();

            final Repository<Car, String> carsRepository = new CarHibernateRepository(emf.createEntityManager());
            final Repository<Animal, String> animalsRepository = new AnimalsRepository(connection);
            //final Repository<Workout, Integer> workoutsRepository = new WorkoutsRepository(connection);
            final WorkoutsHibernateRepository workoutsRepository = new WorkoutsHibernateRepository(
                    emf.createEntityManager());

            final Repository<Manufacturer, String> manufacturersRepository = new ManufacturersHibernateRepository(
                    emf.createEntityManager());

            final Repository<Dog, String> dogsRepository = new DogsHibernateRepository(emf.createEntityManager());
            final Repository<Apartment, String> apartmentsRepository = new ApartmentsHibernateRepository(
                    emf.createEntityManager());
            final Repository<Member, String> membersRepository = new MembersHibernateRepository(
                    emf.createEntityManager());

            boolean isProgramRunning = true;
            while (isProgramRunning) {
                showMenu();
                final String chosenOption = SCANNER.nextLine();
                try {
                    final int option = Integer.parseInt(chosenOption);
                    switch (option) {
                        case 1:
                            carsMenu(carsRepository);
                            break;
                        case 2:
                            animalsMenu(animalsRepository);
                            break;
                        case 3:
                            manufacturersMenu(manufacturersRepository);
                            break;
                        case 4:
                            apartmentsMenu(apartmentsRepository);
                            break;
                        case 5:
                            dogsMenu(dogsRepository);
                            break;
                        case 6:
                            workoutsMenu(workoutsRepository);
                            break;
                        case 7:
                            membersMenu(membersRepository);
                        case 0:
                            isProgramRunning = false;
                            break;
                        default:
                            System.err.println(chosenOption + " is invalid option. Please try again!");
                            break;
                    }
                } catch (NumberFormatException e) {
                    System.err.println(chosenOption + " is invalid option. Please try again!");
                }
            }
        }
    }

    private static void apartmentsMenu(Repository<Apartment, String> apartmentsRepository) throws SQLException {
        boolean isApartmentsMenuRunning = true;
        while (isApartmentsMenuRunning) {
            showApartmentsMenu();
            final String chosenOption = SCANNER.nextLine();
            try {
                final int option = Integer.parseInt(chosenOption);
                switch (option) {
                    case 1:
                        System.out.println("Provide apartment location:");
                        String location = SCANNER.nextLine();
                        System.out.println("Provide apartment metric area:");
                        int metricArea = SCANNER.nextInt();
                        System.out.println("Provide apartment floor:");
                        int floor = SCANNER.nextInt();
                        System.out.println("Provide apartment year of construction:");
                        int yearOfConstruction = SCANNER.nextInt();
                        apartmentsRepository.create(new Apartment(location, metricArea, floor, yearOfConstruction));
                        break;
                    case 2:
                        List<Apartment> apartments = apartmentsRepository.findAll(100);
                        System.out.println(apartments);
                        break;
                    case 3:
                        System.out.println("Provide id of apartment to update:");
                        String id = SCANNER.nextLine();
                        Apartment apartmentToUpdate = apartmentsRepository.findById(id);
                        System.out.println("Apartment to update" + apartmentToUpdate);
                        // ... ?
                        break;
                    case 4:
                        System.out.println("Provide id of apartment to delete:");
                        String idOfApartmentToDelete = SCANNER.nextLine();
                        apartmentsRepository.deleteById(idOfApartmentToDelete);
                        break;
                    case 0:
                        isApartmentsMenuRunning = false;
                        break;
                    default:
                        System.err.println(chosenOption + " is invalid option. Please try again!");
                        break;
                }
            } catch (NumberFormatException e) {
                System.err.println(chosenOption + " is invalid option. Please try again!");
            }
        }
    }

    private static void manufacturersMenu(Repository<Manufacturer, String> manufacturersRepository)
            throws SQLException {
        boolean isManufacturersMenuRunning = true;
        while (isManufacturersMenuRunning) {
            showManufacturersMenu();
            final String chosenOption = SCANNER.nextLine();
            try {
                final int option = Integer.parseInt(chosenOption);
                switch (option) {
                    case 1:
                        System.out.println("Provide manufacturer name:");
                        String manufacturerName = SCANNER.nextLine();
                        manufacturersRepository.create(new Manufacturer(manufacturerName));
                        break;
                    case 2:
                        List<Manufacturer> manufacturers = manufacturersRepository.findAll(100);
                        System.out.println(manufacturers);
                        break;
                    case 3:
                        System.out.println("Provide id of manufacturer to update:");
                        String id = SCANNER.nextLine();
                        Manufacturer manufacturerToUpdate = manufacturersRepository.findById(id);
                        System.out.println("Provide new name of manufacturer:");
                        String newName = SCANNER.nextLine();
                        manufacturerToUpdate.setName(newName);
                        manufacturersRepository.update(manufacturerToUpdate);
                        break;
                    case 4:
                        System.out.println("Provide id of manufacturer to delete:");
                        String idToDelete = SCANNER.nextLine();
                        manufacturersRepository.deleteById(idToDelete);
                        break;
                    case 0:
                        isManufacturersMenuRunning = false;
                        break;
                    default:
                        System.err.println(chosenOption + " is invalid option. Please try again!");
                        break;
                }
            } catch (NumberFormatException e) {
                System.err.println(chosenOption + " is invalid option. Please try again!");
            }
        }
    }

    private static void animalsMenu(Repository<Animal, String> animalsRepository) throws SQLException {
        boolean isAnimalsMenuRunning = true;
        while (isAnimalsMenuRunning) {
            showAnimalsMenu();
            final String chosenOption = SCANNER.nextLine();
            try {
                final int option = Integer.parseInt(chosenOption);
                switch (option) {
                    case 1:
                        System.out.println("Provide animal name:");
                        String name = SCANNER.nextLine();
                        System.out.println("Provide animal type:");
                        String type = SCANNER.nextLine();
                        System.out.println("Provide animal diet:");
                        String diet = SCANNER.nextLine();
                        System.out.println("Provide animal colour:");
                        String colour = SCANNER.nextLine();
                        animalsRepository.create(new Animal(name, type, DietType.valueOf(diet), colour));
                        break;
                    case 2:
                        List<Animal> animals = animalsRepository.findAll(100);
                        System.out.println(animals);
                        break;
                    case 3:
                        System.out.println("Provide id of animal to update:");
                        String id = SCANNER.nextLine();
                        Animal animalToUpdate = animalsRepository.findById(id);
                        System.out.println("Animal to update: " + animalToUpdate);
                        System.out.println("Provide type of diet (o - omnivore, c - carnivore, h - herbivore):");
                        String typeOfDietSelection = SCANNER.nextLine().toLowerCase();
                        switch (typeOfDietSelection) {
                            case "o":
                                animalsRepository.update(
                                        animalToUpdate.copy(animalToUpdate.getAnimalName(), animalToUpdate.getType(),
                                                DietType.OMNIVORE, animalToUpdate.getColour()));
                                break;
                            case "c":
                                animalsRepository.update(
                                        animalToUpdate.copy(animalToUpdate.getAnimalName(), animalToUpdate.getType(),
                                                DietType.CARNIVORE, animalToUpdate.getColour()));
                                break;
                            case "h":
                                animalsRepository.update(
                                        animalToUpdate.copy(animalToUpdate.getAnimalName(), animalToUpdate.getType(),
                                                DietType.HERBIVORE, animalToUpdate.getColour()));
                                break;
                            default:
                                System.out.println("Invalid diet type, no update executed...");
                                break;
                        }
                        break;
                    case 4:
                        System.out.println("Provide id of animal to delete:");
                        String idOfAnimalToDelete = SCANNER.nextLine();
                        animalsRepository.deleteById(idOfAnimalToDelete);
                        break;
                    case 0:
                        isAnimalsMenuRunning = false;
                        break;
                    default:
                        System.err.println(chosenOption + " is invalid option. Please try again!");
                        break;
                }
            } catch (NumberFormatException e) {
                System.err.println(chosenOption + " is invalid option. Please try again!");
            }
        }
    }

    private static void carsMenu(Repository<Car, String> carsRepository) throws SQLException {
        boolean isCarsMenuRunning = true;
        while (isCarsMenuRunning) {
            showCarsMenu();
            final String chosenOption = SCANNER.nextLine();
            try {
                final int option = Integer.parseInt(chosenOption);
                switch (option) {
                    case 1:
                        System.out.println("Provide car manufacturer:");
                        String manufacturer = SCANNER.nextLine();
                        System.out.println("Provide car model:");
                        String model = SCANNER.nextLine();
                        System.out.println("Provide car engine type:");
                        String engineType = SCANNER.nextLine();
                        System.out.println("Provide car year of production:");
                        int yearOfProduction = SCANNER.nextInt();
                        carsRepository.create(
                                new Car(new Manufacturer(manufacturer), model, EngineType.valueOf(engineType),
                                        yearOfProduction));
                        break;
                    case 2:
                        List<Car> cars = carsRepository.findAll(100);
                        System.out.println(cars);
                        break;
                    case 3:
                        System.out.println("Provide id of car to update:");
                        String id = SCANNER.nextLine();
                        Car carToUpdate = carsRepository.findById(id);
                        System.out.println("Car to update: " + carToUpdate);
                        System.out.println(
                                "Provide type of engine (d - diesel, b - gasoline, h - hybrid, e - electric):");
                        String typeOfEngineSelection = SCANNER.nextLine().toLowerCase();
                        switch (typeOfEngineSelection) {
                            case "d":
                                carsRepository.update(
                                        carToUpdate.copy(carToUpdate.getManufacturer(), carToUpdate.getModel(),
                                                EngineType.DIESEL, carToUpdate.getYearOfProduction()));
                                break;
                            case "b":
                                carsRepository.update(
                                        carToUpdate.copy(carToUpdate.getManufacturer(), carToUpdate.getModel(),
                                                EngineType.GASOLINE, carToUpdate.getYearOfProduction()));
                                break;
                            case "h":
                                carsRepository.update(
                                        carToUpdate.copy(carToUpdate.getManufacturer(), carToUpdate.getModel(),
                                                EngineType.HYBRID, carToUpdate.getYearOfProduction()));
                                break;
                            case "e":
                                carsRepository.update(
                                        carToUpdate.copy(carToUpdate.getManufacturer(), carToUpdate.getModel(),
                                                EngineType.ELECTRIC, carToUpdate.getYearOfProduction()));
                                break;
                            default:
                                System.out.println("Invalid engine type, no update executed...");
                                break;
                        }
                        break;
                    case 4:
                        System.out.println("Provide id of car to delete:");
                        String idOfCarToDelete = SCANNER.nextLine();
                        carsRepository.deleteById(idOfCarToDelete);
                        break;
                    case 11:
                        if (carsRepository instanceof CarHibernateRepository) {
                            final CarHibernateRepository repository = (CarHibernateRepository) carsRepository;
                            List<Object[]> results = repository.findCarsManufacturerNameAndModelOnly(100);
                            System.out.println(results.stream().map(e -> "" + e[0] + " " + e[1] + "\n")
                                    .collect(Collectors.toList()));
                            break;
                        }
                        System.err.println("carsRepository is JDBC one, nothing to do. Use hibernate one...");
                        break;
                    case 12:
                        if (carsRepository instanceof CarHibernateRepository) {
                            final CarHibernateRepository repository = (CarHibernateRepository) carsRepository;
                            System.out.println("Provide model to search");
                            String modelToSearch = SCANNER.nextLine();
                            List<Car> results = repository.findCarsByModel(modelToSearch);
                            System.out.println(results);
                            break;
                        }
                        System.err.println("carsRepository is JDBC one, nothing to do. Use hibernate one...");
                        break;
                    case 13:
                        if (carsRepository instanceof CarHibernateRepository) {
                            final CarHibernateRepository repository = (CarHibernateRepository) carsRepository;
                            List<Car> results = repository.findAllOrderByYearOfProduction(100, false);
                            System.out.println(results);
                        }
                        System.err.println("carsRepository is JDBC one, nothing to do. Use hibernate one...");
                        break;
                    case 14:
                        if (carsRepository instanceof CarHibernateRepository) {
                            final CarHibernateRepository repository = (CarHibernateRepository) carsRepository;
                            List<Object[]> results = repository.findAvgYearOfProductionByManufacturers();
                            System.out.println(results.stream().map(e -> "" + e[0] + " " + e[1] + "\n")
                                    .collect(Collectors.toList()));
                            break;
                        }
                        System.err.println("carsRepository is JDBC one, nothing to do. Use hibernate one...");
                        break;
                    case 15:
                        if (carsRepository instanceof CarHibernateRepository) {
                            final CarHibernateRepository repository = (CarHibernateRepository) carsRepository;
                            System.out.println("Provide manufacturer to search by");
                            String manufacturerToSearchBy = SCANNER.nextLine();
                            List<Car> results = repository.findCarsByManufacturer(manufacturerToSearchBy);
                            System.out.println(results);
                            break;
                        }
                        System.err.println("carsRepository is JDBC one, nothing to do. Use hibernate one...");
                        break;
                    case 0:
                        isCarsMenuRunning = false;
                        break;
                    default:
                        System.err.println(chosenOption + " is invalid option. Please try again!");
                        break;
                }
            } catch (NumberFormatException e) {
                System.err.println(chosenOption + " is invalid option. Please try again!");
            }
        }
    }

    private static void dogsMenu(Repository<Dog, String> dogsRepository) throws SQLException {
        boolean isDogsMenuRunning = true;
        while (isDogsMenuRunning) {
            showDogsMenu();
            final String chosenOption = SCANNER.nextLine();
            try {
                final int option = Integer.parseInt(chosenOption);
                switch (option) {
                    case 1:
                        System.out.println("Provide dog name:");
                        String name = SCANNER.nextLine();
                        System.out.println("Provide dog age:");
                        int age = SCANNER.nextInt();
                        SCANNER.nextLine();
                        System.out.println("Provide dog colour:");
                        String colour = SCANNER.nextLine();
                        dogsRepository.create(new Dog(name, age, colour));
                        break;
                    case 2:
                        List<Dog> dogs = dogsRepository.findAll(100);
                        System.out.println(dogs);
                        break;
                    case 3:
                        System.out.println("Provide id of dog to update:");
                        String id = SCANNER.nextLine();
                        Dog dogToUpdate = dogsRepository.findById(id);
                        System.out.println("Dog to update: " + dogToUpdate);
                        System.out.println("Provide name of dog");
                        String nameOfDogToUpdate = SCANNER.nextLine();
                        dogsRepository.update(
                                dogToUpdate.copy(nameOfDogToUpdate, dogToUpdate.getAGE(), dogToUpdate.getCOLOUR()));
                        break;
                    case 4:
                        System.out.println("Provide id of dog to delete:");
                        String idOfDogToDelete = SCANNER.nextLine();
                        dogsRepository.deleteById(idOfDogToDelete);
                        break;
                    case 0:
                        isDogsMenuRunning = false;
                        break;
                    default:
                        System.err.println(chosenOption + " is invalid option. Please try again!");
                        break;
                }
            } catch (NumberFormatException e) {
                System.err.println(chosenOption + " is invalid option. Please try again!");
            }
        }

    }

    private static void showAnimalsMenu() {
        System.out.println("ANIMALS CRUD");
        System.out.println("1 - Create animal");
        System.out.println("2 - Read animals");
        System.out.println("3 - Update animal");
        System.out.println("4 - Delete animal");
        System.out.println("0 - EXIT");
    }

    private static void showApartmentsMenu() {
        System.out.println("APARTMENTS CRUD");
        System.out.println("1 - Create apartment");
        System.out.println("2 - Read apartments");
        System.out.println("3 - Update apartment");
        System.out.println("4 - Delete apartment");
        System.out.println("0 - EXIT");
    }

    private static void showCarsMenu() {
        System.out.println("CARS CRUD");
        System.out.println("1 - Create car");
        System.out.println("2 - Read cars");
        System.out.println("3 - Update car");
        System.out.println("4 - Delete car");
        System.out.println("11 - select HQL");
        System.out.println("12 - HQL - select car by model");
        System.out.println("13 - HQL - find all order by year of production");
        System.out.println("14 - HQL - find avg year of production by manufacturer");
        System.out.println("15 - HQL - select car by manufacturer name");
        System.out.println("0 - EXIT");
    }

    private static void showManufacturersMenu() {
        System.out.println("MANUFACTURERS CRUD");
        System.out.println("1 - Create manufacturer");
        System.out.println("2 - Read manufacturers");
        System.out.println("3 - Update manufacturer");
        System.out.println("4 - Delete manufacturer");
        System.out.println("0 - EXIT");
    }

    private static void showMenu() {
        System.out.println("MENU");
        System.out.println("1 - CARS");
        System.out.println("2 - ANIMALS");
        System.out.println("3 - MANUFACTURERS");
        System.out.println("4 - APARTMENTS");
        System.out.println("5 - DOGS");
        System.out.println("6 - WORKOUTS");
        System.out.println("7 - MEMBERS");
        System.out.println("0 - EXIT");
    }

    private static void showDogsMenu() {
        System.out.println("DOGS CRUD");
        System.out.println("1 - Create dog");
        System.out.println("2 - Read dogs");
        System.out.println("3 - Update dog");
        System.out.println("4 - Delete dog");
        System.out.println("0 - EXIT");
    }

    private static void workoutsMenu(WorkoutsHibernateRepository workoutsRepository) throws SQLException {
        boolean isWorkoutsMenuRunning = true;
        while (isWorkoutsMenuRunning) {
            showWorkoutsMenu();
            final String chosenOption = SCANNER.nextLine();
            try {
                final int option = Integer.parseInt(chosenOption);
                switch (option) {
                    case 1:
                        WorkoutType workoutType = workoutsRepository.getWorkoutTypeFromUserInput();
                        double distance = workoutsRepository.getDistanceFromUserInput();
                        LocalTime time = workoutsRepository.getTimeFromUserInput();
                        LocalDate date = workoutsRepository.getDateFromUserInput();
                        workoutsRepository.create(new Workout(workoutType, distance, time, date));
                        break;
                    case 2:
                        List<Workout> workouts = workoutsRepository.findAll(100);
                        for (Workout w : workouts) {
                            System.out.println(w);
                        }
                        break;
                    case 3:
                        System.out.println("Provide id of workout to update:");
                        int id = SCANNER.nextInt();
                        Workout workoutToUpdate = workoutsRepository.findById(id);
                        System.out.println("Workout to update: " + workoutToUpdate);
                        showWorkoutUpdateMenu();
                        int typeOfUpdateSelection = SCANNER.nextInt();
                        switch (typeOfUpdateSelection) {
                            case 1:
                                try {
                                    WorkoutType updatedWorkoutType = workoutsRepository.getWorkoutTypeFromUserInput();
                                    workoutsRepository.update(
                                            workoutToUpdate.copy(id, updatedWorkoutType, workoutToUpdate.getDistance(),
                                                    workoutToUpdate.getTime(), workoutToUpdate.getDate()), id);
                                    SCANNER.nextLine();
                                } catch (Exception e) {
                                    throw new IllegalArgumentException("Something went wrong, please try again.");
                                }
                                break;
                            case 2:
                                double updatedDistance = workoutsRepository.getDistanceFromUserInput();
                                workoutsRepository.update(
                                        workoutToUpdate.copy(id, workoutToUpdate.getWorkoutType(), updatedDistance,
                                                workoutToUpdate.getTime(), workoutToUpdate.getDate()), id);
                                SCANNER.nextLine();
                                break;
                            case 3:
                                LocalTime updatedTime = workoutsRepository.getTimeFromUserInput();
                                workoutsRepository.update(workoutToUpdate.copy(id, workoutToUpdate.getWorkoutType(),
                                        workoutToUpdate.getDistance(), updatedTime, workoutToUpdate.getDate()), id);
                                SCANNER.nextLine();
                                break;
                            case 4:
                                LocalDate updatedDate = workoutsRepository.getDateFromUserInput();
                                workoutsRepository.update(workoutToUpdate.copy(id, workoutToUpdate.getWorkoutType(),
                                        workoutToUpdate.getDistance(), workoutToUpdate.getTime(), updatedDate), id);
                                SCANNER.nextLine();
                                break;
                            default:
                                System.out.println("Invalid update type, no update executed...");
                                break;
                        }
                        break;
                    case 4:
                        System.out.println("Insert id of workout to delete:");
                        int idOfWorkoutToDelete = SCANNER.nextInt();
                        workoutsRepository.deleteById(idOfWorkoutToDelete);
                        SCANNER.nextLine();
                        break;
                    case 0:
                        isWorkoutsMenuRunning = false;
                        break;
                    default:
                        System.err.println(chosenOption + " is invalid option. Please try again!");
                        break;
                }
            } catch (NumberFormatException e) {
                System.err.println(chosenOption + " is invalid option. Please try again!");
            }
        }
    }

    private static void membersMenu(Repository<Member, String> membersRepository) throws SQLException {
        boolean isMembersMenuRunning = true;
        while (isMembersMenuRunning) {
            showMembersMenu();
            final String chosenOption = SCANNER.nextLine();
            try {
                final int option = Integer.parseInt(chosenOption);
                switch (option) {
                    case 1:
                        System.out.println("Provide member name:");
                        String name = SCANNER.nextLine();
                        System.out.println("Provide email:");
                        String email = SCANNER.nextLine();
                        System.out.println("Provide date of birth:");
                        String birthDate = SCANNER.nextLine();

                        membersRepository.create(new Member(name, email, birthDate));
                        break;
                    case 2:
                        List<Member> members = membersRepository.findAll(100);
                        System.out.println(members);
                        break;
                    case 3:
                        System.out.println("Provide id of member to update:");
                        String id = SCANNER.nextLine();
                        Member memberToUpdate = membersRepository.findById(id);
                        System.out.println("Member to update: " + memberToUpdate);
                        System.out.println("Provide updated name:");
                        String updatedName = SCANNER.nextLine();

                        membersRepository.update(memberToUpdate.copy(updatedName, memberToUpdate.getEmail(),
                                memberToUpdate.getBirthDate()));
                        break;

                    case 4:
                        System.out.println("Provide id of member to delete:");
                        String idOfMemberToDelete = SCANNER.nextLine();
                        membersRepository.deleteById(idOfMemberToDelete);
                        break;
                    case 0:
                        isMembersMenuRunning = false;
                        break;
                    default:
                        System.err.println(chosenOption + " is invalid option. Please try again!");
                        break;
                }
            } catch (NumberFormatException e) {
                System.err.println(chosenOption + " is invalid option. Please try again!");
            }
        }
    }

    private static void showMembersMenu() {
        System.out.println("MEMBERS CRUD");
        System.out.println("1 - Create member");
        System.out.println("2 - Read members");
        System.out.println("3 - Update member");
        System.out.println("4 - Delete member");
        System.out.println("0 - EXIT");
    }

    private static void showWorkoutsMenu() {
        System.out.println("WORKOUTS CRUD");
        System.out.println("1 - Create workout");
        System.out.println("2 - Read workouts");
        System.out.println("3 - Update workout");
        System.out.println("4 - Delete workout");
        System.out.println("0 - EXIT");
    }

    private static void showWorkoutUpdateMenu() {
        System.out.println(
                "Choose which column you would like to update " + "\n 1- Workout type" + "\n 2 - Workout distance "
                        + "\n 3 - Workout time" + "\n 4 - Workout date");
    }

}
