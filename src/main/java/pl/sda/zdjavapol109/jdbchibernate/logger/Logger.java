package pl.sda.zdjavapol109.jdbchibernate.logger;

public interface Logger {

    void log(String logMsg);

    void logError(String logMsg);
}
