package pl.sda.zdjavapol109.jdbchibernate.logger;

import java.time.LocalDateTime;

public class ConsoleLogger implements Logger {

    @Override
    public void log(String logMsg) {
        System.out.println(LocalDateTime.now() + ": " + logMsg);
    }

    @Override
    public void logError(String logMsg) {
        System.err.println(LocalDateTime.now() + ": " + logMsg);
    }
}
