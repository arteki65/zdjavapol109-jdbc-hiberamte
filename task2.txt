1. git pull origin master
 - jeżeli wyskakują błędy to -> git stash
  git pull origin master
2. git checkout -b nazwisko-task2
3. skopiować encję i repozytorium z własnego branche'a task1
4. Dodać odpowiednie adnotacje to klasy reprezentującej tabelę z bazy danych (przykład Manufacturers.java)
5. Dodać do konfiguracji hibernate w Main.java informację o zarządzanej encji (przykład linia 37).
6. Stworzyć klasę repozytorium implementującą interfejs Repository która będzie używała hibernate zamiast JDBC
(przykład ManufacturersHibernateRepository)

Uruchomienie programu
- skopiować plik hibernate.example.cfg.xml do resources/hibernate i zmienić nazwę na hibernate.cfg.xml
- w pliku zmienić url do bazy danych, użytkownika i hasło
